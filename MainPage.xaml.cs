﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Globalization;
using System.Runtime.Serialization.Json;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace StudyTimeMobile
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Конструктор

        public MainPage()
        {
            InitializeComponent();

            // Задайте для контекста данных элемента управления listbox пример данных
            
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);
            
        }
        

        // Загрузка данных для элементов ViewModel
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            String group = "";
            String savedGroup = "";
            Boolean Cached = false;
            var isolatedStore = IsolatedStorageSettings.ApplicationSettings;
            try
            {
                group = isolatedStore["group"].ToString();
            }
            catch
            {
                NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
            }

            try
            {
                savedGroup = isolatedStore["savedGroup"].ToString();
                if (savedGroup.Equals(group))
                {
                    Panorama.Items.Clear();
                    AddPanoramaItem((Day)isolatedStore["Понедельник"]);
                    AddPanoramaItem((Day)isolatedStore["Вторник"]);
                    AddPanoramaItem((Day)isolatedStore["Среда"]);
                    AddPanoramaItem((Day)isolatedStore["Четверг"]);
                    AddPanoramaItem((Day)isolatedStore["Пятница"]);
                    AddPanoramaItem((Day)isolatedStore["Суббота"]);
                    Cached = true;
                }

            }
            catch(Exception)
            {
                
            }
            if (Cached == false)
            {
                Day loading = new Day("loading..");
                AddPanoramaItem(loading);
                try
                {
                    if (group.Equals(""))
                    {
                        throw new Exception();
                    }
                    var webClient = new WebClient();
                    var uri = new Uri("http://ancient-coast-8679.herokuapp.com/api/v1/sched/group/" + group);
                    webClient.DownloadStringCompleted +=
                        new DownloadStringCompletedEventHandler(webClient_DownloadStringCompleted);
                    //                webClient.OpenReadAsync(uri);
                    webClient.DownloadStringAsync(uri);
                }
                catch (Exception)
                {
                    MessageBox.Show("You should select a group!");
                }
            }
        }

        void webClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var days = JsonConvert.DeserializeObject<Dictionary<String, Dictionary<String, List<Lesson>>>>(e.Result);
            Panorama.Items.Clear();
            CreateDay(days["Mon"], "Понедельник");
            CreateDay(days["Tue"], "Вторник");
            CreateDay(days["Wed"], "Среда");
            CreateDay(days["Thu"], "Четверг");
            CreateDay(days["Fri"], "Пятница");
            CreateDay(days["Sat"], "Суббота");
            var isolatedStore = IsolatedStorageSettings.ApplicationSettings;
            isolatedStore["savedGroup"] = isolatedStore["group"];
        }
        public void CreateDay(Dictionary<String, List<Lesson>> data, string name)
        {
            Day day = new Day(name);

            for (int i = 0; i < 10; i++)
            {
                string num = (i+1).ToString();
                try
                {
                    day.Lessons.Add(data[num][0]);
                    if (day.Lessons[i].lecture == 1)
                        day.Lessons[i].name = "[Л]" + day.Lessons[i].name;
                    try
                    {
                        if (day.Lessons[i].half_group == 0)
                        {
                            day.Lessons[i].name += "/" + data[num][1].name;
                        }
                        day.Lessons[i].place[0] += "/" + data[num][1].place[0];

                    }
                    catch (Exception)
                    {
                        
                    }
                    try
                    {
                        day.Lessons[i].place[0] = "↳ " + day.Lessons[i].place[0];
                    }
                    catch
                    {
                    }
                }
                catch (Exception)
                {
                    day.Lessons.Add(new Lesson(){name="---", place = new List<string>()});
                    day.Lessons.ElementAt(i).place.Add(" ");
                }
            }
            for (int i = 9; i >=0; i--)
            {
                if (day.Lessons.ElementAt(i).name.Equals("---"))
                {
                    day.Lessons.RemoveAt(i);
                }
                else
                {
                    break;
                }
                
            }
            var isolatedStore = IsolatedStorageSettings.ApplicationSettings;
            isolatedStore[name] = day;
            AddPanoramaItem(day);
        }
        public void AddPanoramaItem(Day day)
        {
            
            var panoramaItem = new PanoramaItem
            {
                Header = day.Title,
                ContentTemplate = (DataTemplate)Application.Current.Resources["DayTemplate"],
                HeaderTemplate = (DataTemplate)Application.Current.Resources["MyItemHeaderTemplate"],
                Content = day,
                Foreground = new SolidColorBrush(new Color()
                    {
                        A = 255 /*Opacity*/,
                        R = 139 /*Red*/,
                        G = 67 /*Green*/,
                        B = 40 /*Blue*/
                })
            };
            Panorama.Items.Add(panoramaItem);
        }

        private void ApplicationBarMenuItem_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);

            if (NavigationService.CanGoBack)
            {

                while (NavigationService.RemoveBackEntry() != null)
                {

                    NavigationService.RemoveBackEntry();

                }

            }      
        }
    }
}