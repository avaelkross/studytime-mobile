﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;

namespace StudyTimeMobile
{
    public partial class Page1 : PhoneApplicationPage
    {
        public Page1()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Settings_Loaded);
        }
        private void Settings_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var webClient = new WebClient();
                var uri = new Uri("http://ancient-coast-8679.herokuapp.com/api/v1/group/list");
                webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_DownloadStringCompleted);
                webClient.DownloadStringAsync(uri);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "error came here 1");
            }

        }

        void webClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var groups = JsonConvert.DeserializeObject<Dictionary<String, Dictionary<String, Dictionary<String,List<String>>>>>(e.Result);
            MakeGroupList(groups);
        }

        private void MakeGroupList(Dictionary<String, Dictionary<String, Dictionary<String,List<String>>>> data)
        {
            loadSettings.Visibility = Visibility.Collapsed;
            var groups = (from k in data["groups"].Keys from kk in data["groups"][k].Keys from s in data["groups"][k][kk] select s).ToList();
            foreach (string q in groups)
            {
                grouplist.Items.Add(q);
            }
        }

        private void grouplist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var isolatedStore = IsolatedStorageSettings.ApplicationSettings;
            isolatedStore["group"] = e.AddedItems[0].ToString();
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
    }
}