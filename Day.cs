﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace StudyTimeMobile
{
    public class Day
    {
        public Day()
        {
            Title = "";
            Lessons = new ObservableCollection<Lesson>();
        }
        public Day(string name)
        {
            Title = name;
            Lessons = new ObservableCollection<Lesson>();
        }
        public string Title { get; set; }
        public ObservableCollection<Lesson> Lessons { get; set; }
    }
}
