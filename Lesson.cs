﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace StudyTimeMobile
{
    public class Lesson
    {
        public string name { get; set; }
        public List<string> place { get; set; }
        public Professor professor { get; set; }
        public int practice { get; set; }
        public int lecture { get; set; }
        public int half_group { get; set; }
    }
}
